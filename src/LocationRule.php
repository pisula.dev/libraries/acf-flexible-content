<?php

namespace IC\Functionality\ACF\FlexibleContent;

class LocationRule {
	private const RULE_TYPE = 'section';

	public function hooks(): void {
		add_filter( 'acf/location/rule_types', [ $this, 'acf_location_rules_types', ] );
		add_filter( 'acf/location/rule_match', [ $this, 'acf_location_rule_match', ], 10, 4 );
		add_filter( 'acf/location/rule_values', [ $this, 'acf_location_rule_values', ], 10, 2 );
		add_filter( 'acf/location/rule_operators', [ $this, 'acf_location_rules_operators', ], 10, 2 );
	}

	/**
	 * Add new location rule.
	 *
	 * @param mixed $groups .
	 *
	 * @return array
	 */
	public function acf_location_rules_types( $groups ): array {
		$groups[ __( 'Flexible Content', 'acf-flexible-content' ) ][ self::RULE_TYPE ] = __( 'Section', 'acf-flexible-content' );
		$groups[ __( 'Flexible Content', 'acf-flexible-content' ) ]['section-field']   = __( 'Section Field', 'acf-flexible-content' );

		return $groups;
	}

	/**
	 * @param mixed $result      .
	 * @param array $rule        .
	 * @param array $screen      .
	 * @param array $field_group .
	 *
	 * @return bool
	 */
	public function acf_location_rule_match( $result, array $rule, array $screen, array $field_group ): bool {
		if ( $rule['param'] !== self::RULE_TYPE ) {
			return $result;
		}

		if ( ! isset( $screen['post_id'] ) ) {
			return $result;
		}

		$template = wp_doing_ajax() && isset( $_POST['page_template'] ) ? $_POST['page_template'] : get_page_template_slug( $screen['post_id'] );

		return isset( $rule['value'] ) && $template === $rule['value'];
	}

	/**
	 * Add section to choose.
	 *
	 * @param mixed $values .
	 * @param array $rule   .
	 *
	 * @return array
	 */
	public function acf_location_rule_values( $values, array $rule ): array {
		if ( $rule['param'] === self::RULE_TYPE ) {
			return wp_get_theme()->get_page_templates( null, Sections::POST_TYPE );
		}

		return $values;
	}

	/**
	 * Modify rule.
	 *
	 * @param mixed $operators .
	 * @param array $rule      .
	 *
	 * @return array
	 */
	public function acf_location_rules_operators( $operators, array $rule ): array {
		if ( $rule['param'] === self::RULE_TYPE ) {
			return [ '==' => __( 'is equal to', 'acf-flexible-content' ) ];
		}

		return $operators;
	}
}
