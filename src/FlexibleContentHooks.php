<?php

namespace IC\Functionality\ACF\FlexibleContent;

class FlexibleContentHooks {
	public function hooks(): void {
		( new Sections() )->hooks();
		( new Templates() )->hooks();
		( new FieldGroup() )->hooks();
		( new LocationRule() )->hooks();
		( new ModifyLayoutAccess() )->hooks();
		( new LoadTranslations() )->hooks();
	}
}
