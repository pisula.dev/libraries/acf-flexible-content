<?php

namespace IC\Functionality\ACF\FlexibleContent;

class LoadTranslations {

	public function hooks(): void {
		add_action( 'init', [ $this, 'load_textdomain' ], 0 );
	}

	public function load_textdomain(): void {
		$locale = determine_locale();

		$mofile = wp_normalize_path( __DIR__ . "/../lang/acf-flexible-content-$locale.mo" );

		load_textdomain( 'acf-flexible-content', $mofile );
	}
}
