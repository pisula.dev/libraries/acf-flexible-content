<?php

namespace IC\Functionality\ACF\FlexibleContent;

class ModifyLayoutAccess {
	public function hooks(): void {
		add_filter( 'acf/prepare_field/type=flexible_content', [ $this, 'filter_flexible_content_sections', ] );
	}

	/**
	 * @param mixed $field
	 *
	 * @return array
	 */
	public function filter_flexible_content_sections( $field ): array {
		if ( ! isset( $field['layouts'] ) || ! is_array( $field['layouts'] ) ) {
			return $field;
		}

		$excluded_section_names = apply_filters( 'acf/flexible_content/exclude_layouts', [], $this->get_post_type(), $field['layouts'], $field );

		$field['layouts'] = array_filter(
			$field['layouts'],
			static function ( $layout ) use ( $excluded_section_names ) {
				return ! in_array( $layout['name'], $excluded_section_names, true );
			}
		);

		return $field;
	}

	/**
	 * @return string
	 */
	private function get_post_type(): string {
		return $_POST['post_type'] ?? get_current_screen()->post_type ?? '';
	}
}
