<?php

namespace IC\Functionality\ACF\FlexibleContent;

use WP_Post_Type;
use WP_Term;

class FlexibleContent {

	/**
	 * @var int .
	 */
	private int $section_id = 0;

	/**
	 * @param string|null     $selector
	 * @param string|int|null $post_id
	 *
	 * @return void
	 */
	public function render( string $selector = null, $post_id = null ): void {
		if ( ! function_exists( 'have_rows' ) ) {
			return;
		}

		while ( have_rows( $this->get_selector( $selector ), $this->get_post_id( $post_id ) ) ) {
			the_row();
			$this->section_id = 0;

			$layout = get_row_layout();

			if ( $layout === 'section' ) {
				$this->section_id = (int) get_sub_field( 'section_id' );
				$layout           = $this->get_section_layout();
			}

			$status = get_template_part( 'page-sections/' . $layout, null, $this->get_fields() );

			if ( $status === false && current_user_can( 'administrator' ) ) {
				trigger_error( sprintf( __( 'Layout %s not exists', 'acf-flexible-content' ), $layout ) );
			}
		}
	}

	/**
	 * @return string
	 */
	private function get_selector( string $selector = null ): string {
		if ( $selector !== null ) {
			return $selector;
		}

		$selector = 'post_content';

		if ( is_home() ) {
			$selector .= '_archive_post';
		} elseif ( get_queried_object() instanceof WP_Term ) {
			$selector .= '_archive_' . get_queried_object()->taxonomy;
		} elseif ( get_queried_object() instanceof WP_Post_Type ) {
			$selector .= '_archive_' . get_queried_object()->name;
		}

		return $selector;
	}

	/**
	 * @return mixed
	 */
	private function get_post_id( $post_id = null ) {
		if ( $post_id !== null ) {
			return $post_id;
		}

		if ( is_singular() ) {
			return get_the_ID();
		}

		return 'options';
	}

	/**
	 * @return string
	 */
	private function get_section_layout(): string {
		return wp_basename( get_page_template_slug( $this->section_id ), '.php' );
	}

	/**
	 * @return mixed
	 */
	private function get_fields() {
		return $this->section_id ? get_fields( $this->section_id ) : get_row( true );
	}
}
