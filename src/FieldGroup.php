<?php

namespace IC\Functionality\ACF\FlexibleContent;

class FieldGroup {
	public function hooks(): void {
		add_action( 'acf/init', [ $this, 'register_acf_fields' ] );
	}

	public function register_acf_fields(): void {
		$location = [
			[
				[
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'page-templates/flexible-content.php',
				],
			],
			[
				[
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => Templates::POST_TYPE,
				],
			],
		];

		acf_add_local_field_group(
			apply_filters(
				'acf/flexible_content/field_group', [
					'key'            => 'group_61fac6721fecf',
					'title'          => __( 'Flexible Content', 'acf-flexible-content' ),
					'fields'         => [
						'post_content' => [
							'key'          => 'field_61fac67575585',
							'label'        => __( 'Flexible Content', 'acf-flexible-content' ),
							'name'         => 'post_content',
							'type'         => 'flexible_content',
							'required'     => 0,
							'status'       => 1,
							'layouts'      => $this->get_layouts(),
							'button_label' => __( 'Add section', 'acf-flexible-content' ),
						],
					],
					'location'       => apply_filters( 'acf/flexible_content/group_location', $location ),
					'menu_order'     => 0,
					'hide_on_screen' => [
						'the_content',
					],
					'active'         => true,
					'show_in_rest'   => false,
				]
			)
		);
	}

	/**
	 * @return array[]
	 */
	private function get_layouts(): array {
		$layouts = [];

		if ( get_theme_support( 'ic-theme-sections' ) ) {
			$layouts['layout_5da7200a6839d'] = [
				'key'        => 'layout_5da7200a6839d',
				'name'       => 'section',
				'label'      => __( 'Predefined Section', 'acf-flexible-content' ),
				'display'    => 'block',
				'sub_fields' => [
					[
						'key'               => 'field_5da7210d74edf',
						'label'             => __( 'Section', 'acf-flexible-content' ),
						'name'              => 'section_id',
						'prefix'            => 'acf',
						'type'              => 'post_object',
						'value'             => null,
						'menu_order'        => 0,
						'instructions'      => '',
						'required'          => 1,
						'id'                => '',
						'class'             => '',
						'conditional_logic' => 0,
						'wrapper'           => [
							'width' => '',
							'class' => '',
							'id'    => '',
						],
						'status'            => 1,
						'post_type'         => [ Sections::POST_TYPE ],
						'taxonomy'          => '',
						'allow_null'        => 0,
						'multiple'          => 0,
						'return_format'     => 'id',
						'ui'                => 1,
					],
				],
				'min'        => '',
				'max'        => '',
			];
		}

		$field_groups = $this->get_field_groups();

		foreach ( $this->get_templates() as $filename => $title ) {
			$key = substr( md5( $filename ), 0, 13 );

			$section = wp_basename( $filename, '.php' );

			$layouts[ sprintf( "layout_%s", $key ) ] = [
				'key'        => sprintf( "layout_%s", $key ),
				'name'       => $section,
				'label'      => $title,
				'display'    => 'block',
				'sub_fields' => [
					[
						'key'               => sprintf( "field_%s", $key ),
						'label'             => $section,
						'name'              => $title,
						'type'              => 'clone',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => [
							'width' => '',
							'class' => '',
							'id'    => '',
						],
						'clone'             => $field_groups[ $section ] ?? [],
						'display'           => 'seamless',
						'layout'            => 'block',
						'prefix_label'      => 0,
						'prefix_name'       => 0,
					],
				],
				'min'        => '',
				'max'        => '',
			];
		}

		return $layouts;
	}

	/**
	 * @return string[]
	 */
	private function get_templates(): array {
		$templates = wp_get_theme()->get_page_templates( null, Sections::POST_TYPE );

		asort( $templates );

		return $templates;
	}

	/**
	 * @return array<string, string[]>
	 */
	private function get_field_groups(): array {
		$field_groups = [];

		foreach ( acf_get_field_groups() as $field_group ) {
			if ( ( $field_group['location'][0][0]['param'] ?? '' ) !== 'section' ) {
				continue;
			}

			$field_groups[ wp_basename( $field_group['location'][0][0]['value'], '.php' ) ][] = $field_group['key'];
		}

		return $field_groups;
	}
}
