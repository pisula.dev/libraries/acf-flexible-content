<?php

namespace IC\Functionality\ACF\FlexibleContent;

class Sections {
	public const POST_TYPE = 'ic-section';

	public function hooks(): void {
		add_action( 'init', [ $this, 'register_post_type' ] );
	}

	public function register_post_type(): void {
		if ( ! get_theme_support( 'ic-theme-sections' ) ) {
			return;
		}

		register_post_type(
			self::POST_TYPE,
			[
				'label'               => __( 'Sections', 'acf-flexible-content' ),
				'supports'            => [ 'title', 'revisions' ],
				'hierarchical'        => false,
				'public'              => false,
				'show_ui'             => true,
				'show_in_menu'        => 'themes.php',
				'menu_position'       => 5,
				'show_in_admin_bar'   => false,
				'show_in_nav_menus'   => false,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => true,
				'publicly_queryable'  => false,
				'rewrite'             => false,
				'capability_type'     => 'page',
				'show_in_rest'        => false,
			]
		);
	}
}
