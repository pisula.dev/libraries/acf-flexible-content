## [2.2.1] - 2022-12-23
### Fixed
- Exclude layouts improves

## [2.2.0] - 2022-06-20
### Added
- acf/flexible_content/field_group filter
- acf/flexible_content/group_location filter
### Changed
- required status

## [2.1.0] - 2022-05-27
### Added
- support for templates

## [2.0.0] - 2022-05-17
### Added
- tranlations
### Changed
- functionality for sections

## [1.0.2] - 2022-02-15
### Added
- section field

## [1.0.1] - 2022-02-01
### Fixed
- sections load


## [1.0.0] - 2022-02-01
### Added
- initial version
